"use strict";
exports.__esModule = true;
var User = /** @class */ (function () {
    function User(email, name, password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }
    User.prototype.matches = function (another) {
        return another !== undefined && another.email === this.email && another.password === this.password;
    };
    return User;
}());
exports.User = User;
exports.users = {
    "vinicius@atrib.com.br": new User('vinicius@atrib.com.br', 'Vinicius', '123'),
    "luciana@atrib.com.br": new User('lucianaflavia@atrib.com.br', 'Luciana', '123')
};
