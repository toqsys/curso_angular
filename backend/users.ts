export class User {
    constructor(public email: string,
                public name: string,
                private password: string){}

    matches(another: User): boolean{
        return another !== undefined && another.email === this.email && another.password === this.password
    }
}

export const users: {[key:string]: User} = {
    "vinicius@atrib.com.br": new User('vinicius@atrib.com.br', 'Vinicius', '123',),
    "luciana@atrib.com.br": new User('lucianaflavia@atrib.com.br', 'Luciana', '123')
}