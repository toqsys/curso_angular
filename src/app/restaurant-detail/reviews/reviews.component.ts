import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from './../../restaurants/restaurants.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'mt-reviews',
  templateUrl: './reviews.component.html'
  }
)

export class ReviewsComponent implements OnInit {

reviews: Observable<any>

  constructor(private restauratsService: RestaurantsService, 
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.reviews = this.restauratsService.reviewsOfRestaurant(this.route.parent.snapshot.params['id'])
  }

}
