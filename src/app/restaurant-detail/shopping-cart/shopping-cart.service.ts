import { NotificationsService } from './../../shared/messages/notifications.service';
import { Injectable } from '@angular/core';
import { MenuItem } from './../menu-item/menu-item.model';
import { CartItem } from "app/restaurant-detail/shopping-cart/cart-item.model";


@Injectable()
export class ShoppingCartService{
    items: CartItem[] = []

    constructor(private notificationService : NotificationsService){

    }

    clear(){
        this.items = []
    }

    total(): number{
        return this.items
        .map(item => item.value()).reduce((prev, value) => prev+value, 0)
    }

    addItem(item: MenuItem){
        let foundItem = this.items.find((mItem)=>mItem.menuItem.id === item.id)
        if(foundItem){
            this.increaseQty(foundItem)
        }else{
            this.items.push(new CartItem(item))
        }
        this.notificationService.notify(`Você adicionou um item ${item.name}`)
    }

    decreaseQty(item: CartItem){
        item.quantity = item.quantity - 1
        if (item.quantity === 0){
            this.removeItem(item)
        }
    }


    increaseQty(item: CartItem){
        item.quantity = item.quantity + 1
    }

    removeItem(item: CartItem){
        this.items.splice(this.items.indexOf(item), 1)
        this.notificationService.notify(`Você removeu o item ${item.menuItem.name}`)
    }
}