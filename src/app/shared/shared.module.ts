import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, Input, ModuleWithProviders } from '@angular/core';

import { RattingComponent } from './ratting/ratting.component';
import { RadioComponent } from './radio/radio.component';
import { InputComponent } from './input/input.component';
import { CommonModule } from '@angular/common';

import { RestaurantsService } from './../restaurants/restaurants.service';
import { ShoppingCartService } from '../restaurant-detail/shopping-cart/shopping-cart.service';
import { OrderService } from '../order/order.service';
import { SnackbarComponent } from './messages/snackbar/snackbar.component';
import { NotificationsService } from './messages/notifications.service';
import { LoginService } from './../security/login/login.service';


@NgModule({
    declarations: [InputComponent, RadioComponent, RattingComponent, SnackbarComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    exports:[InputComponent, RadioComponent, RattingComponent, CommonModule,
            FormsModule, ReactiveFormsModule, SnackbarComponent]
})
export class SharedModule{
    static forRoot(): ModuleWithProviders{
        return {
            ngModule: SharedModule,
            providers: [ShoppingCartService, RestaurantsService, 
                        OrderService, NotificationsService, LoginService]
        }
    }
}